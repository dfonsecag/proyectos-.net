CREATE PROCEDURE TiposCuentas_Insertar
    @Nombre  NVARCHAR(50),
    @UsuarioId Int 
AS
BEGIN
    SET NOCOUNT ON;

    DECLARE @Orden int ;

    SELECT @Orden = COALESCE(MAX(Orden), 0) + 1
    from TiposCuentas 
    WHERE UsuarioId = @UsuarioId

    INSERT INTO TiposCuentas(Nombre, UsuarioId, Orden)
    VALUES (@Nombre, @UsuarioId, @Orden);

    SELECT SCOPE_IDENTITY();
    
END
GO

-- Se ejecuta con f5 y de esa manera se crea en Stored Procedure Slq Server



CREATE PROCEDURE Transacciones_Insertar
    @UsuarioId Int ,
    @FechaTransaccion date,
    @Monto decimal(18,2),
    @CategoriaId int,
    @CuentaId int,
    @Nota NVARCHAR(1000)
   
AS
BEGIN
    SET NOCOUNT ON;

    INSERT INTO Transacciones(UsuarioId, FechaTransaccion, Monto,CategoriaId, CuentaId, Nota)
    VALUES (@UsuarioId, @FechaTransaccion, ABS(@Monto),@CategoriaId, @CuentaId, @Nota);
    
    UPDATE Cuentas 
    set Balance +=@Monto
    WHERE Id = @CuentaId;

    SELECT SCOPE_IDENTITY();
    
END
GO


CREATE PROCEDURE Transacciones_Actualizar
    @Id INT,   
    @FechaTransaccion date,
    @Monto decimal(18,2),   
    @MontoAnterior decimal(18,2),   
    @CuentaId int,
    @CuentaAnteriorId int,
    @CategoriaId int,
    @Nota NVARCHAR(1000) = NULL   
AS
BEGIN
    SET NOCOUNT ON;

    --Revertir la transaccion anterior
    UPDATE Cuentas
    SET Balance -= @MontoAnterior
    WHERE Id = @CuentaAnteriorId

    --Realizar la nueva transaccion
    UPDATE Cuentas
    SET Balance += @Monto
    WHERE Id = @CuentaId

    UPDATE Transacciones
    SET Monto = ABS(@Monto), FechaTransaccion = @FechaTransaccion,
    CategoriaId = @CategoriaId, CuentaId = @CuentaId, Nota = @Nota
    WHERE Id = @Id;
    
END
GO

CREATE PROCEDURE Transacciones_Borrar
    @Id INT
AS
BEGIN
    SET NOCOUNT ON;

    DECLARE @Monto DECIMAL(18,2);
    DECLARE @CuentaId int;
    DECLARE @TipoOperacionId int;

    SELECT @Monto = Monto, @CuentaId = CuentaId, @TipoOperacionId = cat.TipoOperacionId
    from Transacciones
    inner JOIN Categorias cat on cat.Id = Transacciones.CategoriaId
    where Transacciones.Id = @Id;
    
    DECLARE @FactorMultiplicativo int = 1;

    if(@TipoOperacionId = 2)
        SET @FactorMultiplicativo = -1;

    SET @Monto = @Monto * @FactorMultiplicativo;

    UPDATE Cuentas
    set Balance -= @Monto
    WHERE Id = @CuentaId;

    DELETE Transacciones 
    WHERE Id = @Id;    

    
END
GO


