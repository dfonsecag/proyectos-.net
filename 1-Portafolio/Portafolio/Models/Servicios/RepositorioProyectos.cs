using Portafolio.Models;
namespace Portafolio.Servicios
{
    // Generando Una Interfax
    public interface IRepositorioProyectos
    {
        List<Proyecto> ObtenerProyectos();
    }
    public class RepositorioProyectos:IRepositorioProyectos
    {
        public  List<Proyecto> ObtenerProyectos()
        {
        return new List<Proyecto>() {
            new Proyecto{
                Titulo = "Amazon",
                Descripcion = "E realizado en React js",
                Link = "http://amazon.com",
                ImagenUrl = "/imagenes/amazon.png"
            },
             new Proyecto{
                Titulo = "La Lydia",
                Descripcion = "E realizado en Node js",
                Link = "http://amazon.com",
                ImagenUrl = "/imagenes/amazon.png"
            },
             new Proyecto{
                Titulo = "SVM",
                Descripcion = "E realizado en Ruby on Rails",
                Link = "http://amazon.com",
                 ImagenUrl = "/imagenes/amazon.png"
            },
             new Proyecto{
                Titulo = "Teams",
                Descripcion = "E realizado en React Native js",
                Link = "http://amazon.com",
                 ImagenUrl = "/imagenes/amazon.png"
            },
        };
        
    }
    }
}