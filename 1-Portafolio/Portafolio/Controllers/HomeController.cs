﻿using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using Portafolio.Models;
using Portafolio.Servicios;

namespace Portafolio.Controllers;

public class HomeController : Controller
{
    private readonly ILogger<HomeController> _logger;
    private readonly IRepositorioProyectos repositorioProyectos;
    private readonly IServicioEmail servicioEmail;

    public HomeController(ILogger<HomeController> logger,
     IRepositorioProyectos repositorioProyectos, IServicioEmail servicioEmail
    

     )
    {
        _logger = logger;
        this.repositorioProyectos = repositorioProyectos;
        this.servicioEmail = servicioEmail;
    }

    public IActionResult Index()
    {
        // // ViewBag.Nombre = "Diego Garcia Fonseca";
        // var persona = new Persona()
        // {
        //     Nombre ="Diego Garcia Fonseca",
        //     Edad = 26
        // };
        // var repositorioProyectos =  new RepositorioProyectos(); lo cambie instancie de Program con inyeccion de dependencias

        var proyectos = repositorioProyectos.ObtenerProyectos().Take(4).ToList();

       
        var modelo = new HomeIndexViewModel(){ 
            Proyectos = proyectos 

        };
        return View(modelo);
    }
   

    public IActionResult Proyectos()
    {
        var proyectos = repositorioProyectos.ObtenerProyectos();

        return View(proyectos);
    }
    public IActionResult Contacto()
    {
        return View();
    }
    // Atributo
    [HttpPost]
    public async Task<IActionResult> Contacto(ContactoViewModel contactoViewModel)
    {
        // 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
        await servicioEmail.Enviar(contactoViewModel);
        return RedirectToAction("Gracias");
    }

     public IActionResult Gracias()
    {
        return View();
    }


    [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
    public IActionResult Error()
    {
        return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
    }
}
